import { serial as it } from 'ava';
import request from 'supertest';
import app from '../../src/app';

it('GET / should respond with an OK status', (t) => {
  request(app)
    .get('/health')
    .expect(200, { status: 'OK' }, t.pass);
});
