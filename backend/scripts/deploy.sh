#!/usr/bin/env bash

RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

app_name=$1
app_version=$2

echo -e "${YELLOW}Hey, did you bump the version before doing this? (type the option number)${NC}"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) break;;
        No ) exit;;
    esac
done

echo -e "${GREEN}Cool cool cool. Then, let's do this!${NC}"

echo "Making sure packages are installed..."
yarn || exit 1

echo "Building app..."
yarn build || exit 1

echo "Creating package..."
rm -r bundle
mkdir bundle
cp -r dist bundle/
cp -r scripts bundle/
cp .env.example bundle/
cp package.json bundle/

cd bundle
npm i --only=prod

tar -zcf ../${app_name}.${app_version}.tar.gz .

echo "Cleaning up..."
cd ..
rm -r bundle dist

echo -e "${GREEN}All done!${NC} 🍻"