/* eslint-disable import/prefer-default-export */
import session from 'express-session';
import Keycloak from 'keycloak-connect';

import config from '../config';

let keycloak: Keycloak.Keycloak;

const store = new session.MemoryStore();

export const getSessionStore = () => store;

export const getKeycloak = () => {
  if (!keycloak) {
    keycloak = new Keycloak(
      { store },
      {
        realm: config.security.keycloak.realm, // @ts-ignore
        realmPublicKey: config.security.keycloak.realmPublicKey,
        serverUrl: config.security.keycloak.serverUrl,
        clientId: config.security.keycloak.clientId,
        bearerOnly: true,
      },
    );
  }
  return keycloak;
};
