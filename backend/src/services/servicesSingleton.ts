import { Services } from './definitions';
import TM from './implementations/TM';

class ServicesSingleton {
  services?: Services;

  getServices = (): Services => {
    if (!this.services) {
      this.services = {
        taskManager: new TM(),
      };
    }

    return this.services;
  }
}

const servicesSingleton = new ServicesSingleton();

export default servicesSingleton;
