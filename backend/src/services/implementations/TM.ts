import config from '../../config';
import { post } from '../../utils/http';

import { TaskManager } from '../definitions';

class TM implements TaskManager {
  registerTask = async (userIds: Array<string>, taskId: string): Promise<void> => {
    await post(config.services.TM.url, '/tasks', {
      taskId,
      userIds,
    }, { apiKey: config.services.TM.apiKey });
  }
}

export default TM;
