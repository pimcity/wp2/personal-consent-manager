export interface TaskManager {
  registerTask(userIds: Array<string>, taskId: string): Promise<void>;
}

export interface Services {
  taskManager: TaskManager;
}
