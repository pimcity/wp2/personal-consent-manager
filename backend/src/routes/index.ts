import Router from 'express-promise-router';
import session from 'express-session';

import decorateUserRequest from '../middlewares/decorateUserRequest';
import validateInternalAuthentication from '../middlewares/internalAuth';
import { getKeycloak, getSessionStore } from '../services/Keycloak';

import health from './health';
import consents from './consents';
import certificates from './certificates';

const keycloak = getKeycloak();
const store = getSessionStore();

const router = Router();

router.use(session({
  secret: 'some secret',
  resave: false,
  saveUninitialized: true,
  store,
}));
router.use(keycloak.middleware());

router.use('/health', health);
router.use('/certificates', validateInternalAuthentication, certificates);
router.use('/consents', keycloak.protect('end-user'), decorateUserRequest, consents);

export default router;
