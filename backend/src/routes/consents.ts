import Router from 'express-promise-router';

import { run } from '../middlewares/run';
import { getConsents, updateConsent } from '../operations/globalConsents/consents';
import { getIABConsents, updateIABConsent } from '../operations/iabConsents/consents';
import servicesSingleton from '../services/servicesSingleton';
import { AuthRequest } from '../types/middlewares/auth';

const router = Router();

const services = servicesSingleton.getServices();

router.get('/', run(getConsents, {
  params: (req: AuthRequest) => [req.user.id],
}));

router.put('/', run(updateConsent, {
  params: (req: AuthRequest) => [req.user.id, req.body.consentId, req.body.active, services],
}));

router.get('/iab', run(getIABConsents, {
  params: (req: AuthRequest) => [req.user.id],
}));

router.put('/iab', run(updateIABConsent, {
  params: (req: AuthRequest) => [req.user.id, req.body.iabCode, req.body.active],
}));

export default router;
