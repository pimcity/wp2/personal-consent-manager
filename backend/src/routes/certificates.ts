import { Request } from 'express';
import Router from 'express-promise-router';
import { getSignedCertificate } from '../operations/globalConsents/certificates';
import { run } from '../middlewares/run';

const router = Router();

router.post('/data-sharing', run(getSignedCertificate, {
  params: (req: Request) => [req.body.dataTypes, req.body.purpose],
}));

export default router;
