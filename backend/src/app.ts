import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import cors from 'cors';
import boom from 'express-boom';
import * as OpenApiValidator from 'express-openapi-validator';
import swaggerUi from 'swagger-ui-express';
import YAML from 'yamljs';

import config from './config';
import logger from './utils/logger';
import errorHandler from './middlewares/errorHandler';
import routes from './routes';

const app = express();

app.use(boom());
app.use(helmet());
app.use(express.json());
app.use(morgan('combined', {
  stream: logger.stream,
  skip: (req) => {
    if (req.originalUrl === '/health') return true;
    if (config.env === 'test') return true;
    return false;
  },
}));
app.use(cors());

const schemaPath = `${__dirname}/schema.yml`;
const schema = YAML.load(schemaPath);

app.use(OpenApiValidator.middleware({ apiSpec: schemaPath }));

app.use('/api-docs', (req: express.Request, res: express.Response, next: express.NextFunction) => {
  if (req.originalUrl === '/api-docs') {
    return res.redirect('./api-docs/');
  }
  if (config.env === 'development') {
    schema.servers[0].url = `http://${req.get('host')}`;
  }
  // @ts-ignore
  req.swaggerDoc = schema;
  return next();
}, swaggerUi.serve, swaggerUi.setup());

app.use((error: Error, req: any, res: any, next: any) => { throw error; });
app.use(routes);
app.use(errorHandler); // This MUST always go after any other app.use(...)

export default app;
