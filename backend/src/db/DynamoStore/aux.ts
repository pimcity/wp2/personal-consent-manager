import R from 'ramda';
import DynamoDB, { DocumentClient, ExpressionAttributeNameMap, Key } from 'aws-sdk/clients/dynamodb';
import logger from '../../utils/logger';
import config from '../../config';
import { DeepMap, DeepPartial } from '../../types/miscellaneous';
import isObject from '../../utils/isObject';

const dynamodb = new DynamoDB(config.aws.dynamo);
export const docClient = new DynamoDB.DocumentClient({ service: dynamodb });

const getTableInfo = async (TableName: DynamoDB.TableName) => {
  try {
    const info = await dynamodb.describeTable({ TableName }).promise();
    return info.Table;
  } catch (err) {
    return undefined;
  }
};

const createTable = async (
  TableName: DynamoDB.TableName,
  KeySchema: DynamoDB.KeySchema,
  AttributeDefinitions: DynamoDB.AttributeDefinitions,
  GlobalSecondaryIndexes?: DynamoDB.GlobalSecondaryIndexList,
) => {
  const params: DynamoDB.CreateTableInput = {
    TableName,
    KeySchema,
    AttributeDefinitions,
    GlobalSecondaryIndexes,
    BillingMode: 'PAY_PER_REQUEST',
  };
  logger.info('Creating table with schema:');
  logger.info(JSON.stringify(params));

  await dynamodb.createTable(params).promise();
};

const updateTable = async (
  TableName: DynamoDB.TableName,
  AttributeDefinitions: DynamoDB.AttributeDefinitions,
  GlobalSecondaryIndexUpdates: DynamoDB.GlobalSecondaryIndexUpdateList,
) => {
  const params: DynamoDB.UpdateTableInput = {
    TableName,
    AttributeDefinitions,
    GlobalSecondaryIndexUpdates,
  };
  logger.info('Updating table with schema:');
  logger.info(JSON.stringify(params));

  await dynamodb.updateTable(params).promise();
};

const diffGlobalIndex = (
  newIndexes?: DynamoDB.GlobalSecondaryIndexList,
  oldIndexes?: DynamoDB.GlobalSecondaryIndexDescriptionList,
): DynamoDB.GlobalSecondaryIndexUpdateList => {
  const creations = newIndexes?.filter(
    (newInd) => !oldIndexes || !oldIndexes.find((oldInd) => oldInd.IndexName === newInd.IndexName),
  ).map((ind) => ({ Create: ind }) as DynamoDB.GlobalSecondaryIndexUpdate) || [];

  // updates not supported

  const deletes = oldIndexes?.filter(
    (oldInd) => !newIndexes || !newIndexes.find((newInd) => oldInd.IndexName === newInd.IndexName),
  ).map((ind) => ({ Delete: ind }) as DynamoDB.GlobalSecondaryIndexUpdate) || [];

  return [...creations, ...deletes];
};

export const upsertTable = async (
  TableName: DynamoDB.TableName,
  KeySchema: DynamoDB.KeySchema = [{ AttributeName: 'id', KeyType: 'HASH' }],
  AttributeDefinitions: DynamoDB.AttributeDefinitions = [{ AttributeName: 'id', AttributeType: 'S' }],
  GlobalSecondaryIndexes?: DynamoDB.GlobalSecondaryIndexList,
  updateIndexes = true,
) => {
  const info = await getTableInfo(TableName);
  if (info) {
    const diff = diffGlobalIndex(GlobalSecondaryIndexes, info.GlobalSecondaryIndexes);
    if (diff.length > 0 && updateIndexes) {
      await updateTable(TableName, AttributeDefinitions, diff);
    }
  } else {
    await createTable(TableName, KeySchema, AttributeDefinitions, GlobalSecondaryIndexes);
  }
};

/**
 * Buils the map of expression attributes to real attribute names using
 * the keys in the data map, e.g.:
 * Input:
 * {
 *   level1a: {
 *      level2a: value1a2a
 *   },
 *   level1b: value1b
 * }
 * Output:
 * {
 *   #level1a: level1a,
 *   #level2a: level2a,
 *   #level1b: level1b,
 * }
 */
const buildExpressionAttributeNames = (
  data: DeepPartial<any>,
): ExpressionAttributeNameMap => Object.entries(data).filter(
  ([, value]) => value !== null && value !== undefined,
).reduce(
  (soFar, [key, value]) => ({
    ...soFar,
    [`#${key}`]: key,
    ...(isObject(value) ? buildExpressionAttributeNames(value) : {}),
  }),
  {} as ExpressionAttributeNameMap,
);

/**
 * Converts a deep object into a flat object where each key has a dot per level, e.g.:
 * Input:
 * {
 *   level1a: {
 *      level2a: value1a2a
 *   },
 *   level1b: value1b
 * }
 * Output:
 * {
 *   level1a.level2a: value1a2a,
 *   level1b: value1b
 * }
 */
const flattenEntries = (data: DeepPartial<any>, prefix: string = ''): DeepPartial<any> => Object.entries(data).reduce(
  (soFar, [key, value]) => {
    const mapKey = `${prefix}#${key}`;
    if (isObject(value)) return { ...soFar, ...flattenEntries(value as DeepPartial<any>, `${mapKey}.`) };
    return { ...soFar, [mapKey]: value };
  },
  {} as DeepPartial<any>,
);

/**
 * Builds the expressions needed for dynamo, e.g.:
 * ExpressionAttributeNames: { "#name": "name", "#subscription": "subscription", "#plan": "plan" }
 * ExpressionAttributeValues: { ":newname": "Wibson", ":newsubscriptionplan": "tier1" }
 */
const createExpressions = <T>(primaryKey: string, data: DeepPartial<T>) => {
  const cleanData = R.omit([primaryKey])(data) as DeepPartial<T>;
  const flatData = flattenEntries(cleanData);

  const filteredEntries = Object.entries(flatData).filter(
    ([, value]) => value !== null && value !== undefined,
  ).map(([key, value]) => [key.replace(/[#.]/g, ''), key, value] as [string, string, T]);

  const ExpressionAttributeNames = buildExpressionAttributeNames(cleanData);

  const ExpressionAttributeValues = filteredEntries.reduce(
    (values, [cleanKey,, value]) => ({
      ...values,
      [`:new${cleanKey}`]: value,
    }),
    {},
  );

  return { filteredEntries, ExpressionAttributeNames, ExpressionAttributeValues };
};

/**
 * Builds the expressions needed to carry out an upsert, e.g.:
 * UpdateExpression: 'set #name = :newname, #subscription.#plan = :newsubscriptionplan'
 * ExpressionAttributeNames: { "#name": "name", "#subscription": "subscription", "#plan": "plan" }
 * ExpressionAttributeValues: { ":newname": "Wibson", ":newsubscriptionplan": "tier1" }
 */
export const createUpdateExpressions = <T>(primaryKey: string, data: DeepPartial<T>) => {
  const {
    filteredEntries, ExpressionAttributeNames, ExpressionAttributeValues,
  } = createExpressions(primaryKey, data);

  const UpdateExpression = filteredEntries.reduce(
    (expr, [cleanKey, key]) => `${expr}${expr !== 'set' ? ',' : ''} ${key} = :new${cleanKey}`,
    'set',
  );

  return { UpdateExpression, ExpressionAttributeNames, ExpressionAttributeValues };
};

export const createRemovePropsExpressions = <T>(primaryKey: string, data: DeepMap<T, boolean>) => {
  const {
    filteredEntries, ExpressionAttributeNames,
  } = createExpressions(primaryKey, data as DeepPartial<T>);

  const UpdateExpression = filteredEntries.reduce(
    (expr, [, key]) => `${expr}${expr !== 'remove' ? ',' : ''} ${key}`,
    'remove',
  );

  return { UpdateExpression, ExpressionAttributeNames };
};

export const createIncrementValuesExpressions = <T>(
  primaryKey: string,
  data: DeepMap<T, number>,
) => {
  const {
    filteredEntries, ExpressionAttributeNames, ExpressionAttributeValues,
  } = createExpressions(primaryKey, data as DeepPartial<T>);

  const UpdateExpression = filteredEntries.reduce(
    (expr, [cleanKey, key]) => `${expr}${expr !== 'set' ? ',' : ''} ${key} = ${key} + :new${cleanKey}`,
    'set',
  );

  return { UpdateExpression, ExpressionAttributeNames, ExpressionAttributeValues };
};

export const createQueryExpressions = <T>(primaryKey: string, filterObj: DeepPartial<T>) => {
  const {
    filteredEntries, ExpressionAttributeNames, ExpressionAttributeValues,
  } = createExpressions(primaryKey, filterObj);

  const FilterExpression = filteredEntries.reduce(
    (expr, [cleanKey, key]) => `${expr}${expr.length > 0 ? ' AND ' : ''}${key} = :new${cleanKey}`,
    '',
  );

  return { FilterExpression, ExpressionAttributeNames, ExpressionAttributeValues };
};

/** Executes the query in the store */
export const query = async <T>(params: DocumentClient.QueryInput): Promise<Array<T>> => {
  const results = await docClient.query({
    ...params,
  }).promise();
  return (results.Items || []) as Array<T>;
};

export const scan = async <T>(params: DocumentClient.ScanInput): Promise<Array<T>> => {
  let done = false;
  let lastKey: Key | undefined;
  let items: Array<T> = [];
  while (!done) {
    // eslint-disable-next-line no-await-in-loop
    const result = await docClient.scan({
      ...params,
      ExclusiveStartKey: lastKey,
    }).promise();
    lastKey = result.LastEvaluatedKey;
    done = !result.LastEvaluatedKey;
    items = [...items, ...result.Items as Array<T>];
  }
  return items;
};

export const primaryKeySchemaDefault: DynamoDB.KeySchema = [{ AttributeName: 'id', KeyType: 'HASH' }];
export const attributeDefinitionsDefault: DynamoDB.AttributeDefinitions = [{ AttributeName: 'id', AttributeType: 'S' }];
