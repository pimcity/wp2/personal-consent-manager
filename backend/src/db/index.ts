import { Consent, IABConsent } from './models';
import DynamoStore from './DynamoStore';
import logger from '../utils/logger';

export const consents = new DynamoStore<Consent>('Consents', undefined,
  [
    { AttributeName: 'id', AttributeType: 'S' },
    { AttributeName: 'userId', AttributeType: 'S' },
  ],
  [
    {
      IndexName: 'userId-index',
      KeySchema: [
        { KeyType: 'HASH', AttributeName: 'userId' },
      ],
      Projection: { ProjectionType: 'ALL' },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10,
      },
    },
  ]);

export const iabConsents = new DynamoStore<IABConsent>('IABConsents', undefined,
  [
    { AttributeName: 'id', AttributeType: 'S' },
    { AttributeName: 'userId', AttributeType: 'S' },
  ],
  [
    {
      IndexName: 'userId-index',
      KeySchema: [
        { KeyType: 'HASH', AttributeName: 'userId' },
      ],
      Projection: { ProjectionType: 'ALL' },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10,
      },
    },
  ]);

export const initStores = async () => {
  logger.info('Initializing stores...');

  await consents.init();
  await iabConsents.init();

  logger.info('Stores initialized!');
};
