/* **********
 * Models
 * ********** */

export type Purpose = 'commercial-purpose' | 'research';
export type DataType = 'contact-information' | 'personal-information' | 'browsing-history' | 'location-history' | 'social-networks-data' | 'financial-data' | 'interests';

export interface Consent {
  id: string; // db unique id
  userId: string;
  date: number;
  consentId: string;
  dataType: DataType;
  purpose: Purpose;
  active: boolean;
}

export interface AvailableConsent {
  consentId: string;
  dataType: DataType;
  purpose: Purpose;
}

export interface IABConsent {
  id: string; // db unique id
  userId: string;
  iabCode: string;
  category: string;
  active: boolean;
  date: number;
}

export interface IABCategory {
  iabCode: string;
  tier: 'Tier 1' | 'Tier 2' | 'Tier 3';
  category: string;
}
