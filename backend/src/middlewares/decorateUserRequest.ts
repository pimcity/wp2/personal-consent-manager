import { Request, Response, NextFunction } from 'express';

import { AuthRequest } from '../types/middlewares/auth';

const decorateUserRequest = (req: Request, res: Response, next: NextFunction) => {
  const decoratedReq = req as AuthRequest;
  decoratedReq.user = { // @ts-ignore
    id: req.kauth.grant.access_token.content.sub,
  };
  next();
};

export default decorateUserRequest;
