/* eslint-disable import/prefer-default-export */
import { Request, Response, NextFunction } from 'express';
import { Options } from '../types/middlewares/run';
import { ApiError } from '../utils/error';

const getBody = (req: Request) => [req.body];

/**
 * @function run Invokes operation and responds its result
 * @param {function} operation Function that runs the bussines logic
 * @param {Options} options
 */
export const run = (operation: Function, options: Options = {}) => async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const {
    status = 200,
    emptyStatus = 204,
    ErrorType = ApiError,
    params = getBody,
    silent = false,
  } = options;

  try {
    const response = await operation(...params(req));
    const responseStatus = response !== undefined && response !== null ? status : emptyStatus;
    res.status(responseStatus).json(response);
  } catch (error) {
    next(
      !(error as any).send && error instanceof Error
        ? new ErrorType(error.message, silent)
        : error,
    );
  }
};
