import fetch from 'node-fetch';

export interface Authentication {
  token?: string;
  apiKey?: string;
}

type Authorization = { Authorization: string; } | { 'X-API-Key': string; };

const buildUrl = (baseUrl: string, path: string) => `${baseUrl.replace(/\/$/, '')}/${path.replace(/^\//, '')}`;

const getAuthorization = (auth?: Authentication): Authorization | undefined => {
  if (auth?.token) return { Authorization: `Bearer ${auth?.token}` };
  if (auth?.apiKey) return { 'X-API-Key': auth.apiKey };
  return undefined;
};

export const get = async <R>(baseUrl: string, path: string, auth?: Authentication): Promise<R> => {
  const res = await fetch(buildUrl(baseUrl, path), {
    method: 'GET',
    headers: {
      ...getAuthorization(auth),
      'Content-Type': 'application/json',
    },
  });
  return res.json();
};

export const post = async <T, R>(
  baseUrl: string, path: string, body: T, auth?: Authentication,
): Promise<R | {}> => {
  const res = await fetch(
    buildUrl(baseUrl, path),
    {
      method: 'POST',
      headers: {
        ...getAuthorization(auth),
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    },
  );
  if (res.status >= 400) {
    throw new Error(await res.text());
  }
  if (res.size === 0) return {};
  return res.json();
};

export const postForm = async <T, R>(
  baseUrl: string, path: string, body: T, auth?: Authentication,
): Promise<R> => {
  const formBody = Object.entries(body).map(([key, value]) => {
    const encodedKey = encodeURIComponent(key);
    const encodedValue = encodeURIComponent(value);
    return `${encodedKey}=${encodedValue}`;
  }).join('&');

  const res = await fetch(buildUrl(baseUrl, path), {
    method: 'POST',
    headers: {
      ...getAuthorization(auth),
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    },
    body: formBody,
  });
  return res.json();
};
