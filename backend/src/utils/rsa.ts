import forge from 'node-forge';

export const createPrivateKey = async () => new Promise<string>((resolve, reject) => {
  // generate an RSA key pair asynchronously (uses web workers if available)
  // use workers: -1 to run a fast core estimator to optimize # of workers
  // *RECOMMENDED*: Can be significantly faster than sync. Will use native
  // Node.js 10.12.0+ or WebCrypto API if possible.
  forge.pki.rsa.generateKeyPair({ bits: 2048, workers: 2 }, (err, keypair) => {
    if (err) reject(err);
    else resolve(forge.pki.privateKeyToPem(keypair.privateKey));
  });
});

export const getPublicKey = (privateKey: string) => {
  // convert PEM-formatted private key to a Forge private key
  const forgePrivateKey = forge.pki.privateKeyFromPem(privateKey);

  // get a Forge public key from the Forge private key
  const forgePublicKey = forge.pki.rsa.setPublicKey(forgePrivateKey.n, forgePrivateKey.e);

  // convert the Forge public key to a PEM-formatted public key
  return forge.pki.publicKeyToPem(forgePublicKey);
};

export const sign = (message: any, privateKey: string) => {
  const md = forge.md.sha1.create();
  md.update(JSON.stringify(message), 'utf8');
  const forgePrivateKey = forge.pki.privateKeyFromPem(privateKey);
  return forgePrivateKey.sign(md);
};
