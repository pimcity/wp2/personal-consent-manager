/* eslint-disable max-classes-per-file */
import { Response } from 'express';

export const INTERNAL_ERROR = 'internal_error';
export const INVALID_OPERATION = 'invalid_operation';
export const DATA_NOT_FOUND = 'data_not_found';
export const DATA_CONFLICT = 'data_conflict';
export const REQUEST_RATE_LIMIT = 'request_rate_limit';
export const ERROR_LEVEL_CRIT = 'error_level_crit';
export const RESOURCE_NOT_AVAILABLE = 'resource_not_available';
export const AUTHENTICATION_FAILED = 'authentication_failed';
export const FORBIDDEN = 'forbidden';
export const NOT_IMPLEMENTED = 'not_implemented';

export class ApiError extends Error {
  code: string;

  silent: boolean;

  constructor(message: string, silent: boolean = false, code: string = INTERNAL_ERROR) {
    super(message);
    this.code = code;
    this.silent = silent;
  }

  static from({ message, silent, code }: { message: string, silent: boolean, code: string }) {
    return new this(message, silent, code);
  }

  send(res: Response) {
    res.boom.badData(this.message, { code: this.code });
  }
}

export class NotFoundError extends ApiError {
  constructor(message: string, silent: boolean = false, code: string = DATA_NOT_FOUND) {
    super(message, silent, code);
  }

  send(res: Response) {
    res.boom.notFound(this.message, { code: this.code });
  }
}

export class ConflictError extends ApiError {
  constructor(message: string, silent: boolean = false, code: string = DATA_CONFLICT) {
    super(message, silent, code);
  }

  send(res: Response) {
    res.boom.conflict(this.message, { code: this.code });
  }
}

export class RateLimitError extends ApiError {
  level: string;

  constructor(
    message: string,
    silent: boolean = false,
    code: string = REQUEST_RATE_LIMIT,
    level: string = ERROR_LEVEL_CRIT,
  ) {
    super(message, silent, code);
    this.level = level;
  }

  send(res: Response) {
    res.boom.tooManyRequests(this.message, { code: this.code });
  }
}

export class UnauthorizedError extends ApiError {
  constructor(message: string, silent: boolean = true, code: string = AUTHENTICATION_FAILED) {
    super(message, silent, code);
  }

  send(res: Response) {
    res.boom.unauthorized(this.message, undefined, { code: this.code });
  }
}

export class ForbiddenError extends ApiError {
  constructor(message: string, silent: boolean = false, code: string = FORBIDDEN) {
    super(message, silent, code);
  }

  send(res: Response) {
    res.boom.forbidden(this.message);
  }
}

export class NotImplementedError extends ApiError {
  constructor(message: string, silent: boolean = false, code: string = NOT_IMPLEMENTED) {
    super(message, silent, code);
  }

  send(res: Response) {
    res.boom.notImplemented(this.message);
  }
}
