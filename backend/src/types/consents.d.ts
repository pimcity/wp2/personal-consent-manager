import { DataType } from '../db/models';

export interface ConsentDTO {
  consentId: string;
  dataType: DataType;
  purpose: string;
  active: boolean;
  date?: number;
}

export interface IABConsentDTO {
  iabCode: string;
  tier: 'Tier 1' | 'Tier 2' | 'Tier 3';
  category: string;
  active: boolean;
  date?: number;
}
