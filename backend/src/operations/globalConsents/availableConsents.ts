import { AvailableConsent, Purpose } from '../../db/models';
import availableDataTypes from './availableDataTypes';

const availablePurposes: Array<Purpose> = [
  'commercial-purpose',
  'research',
];

const availableConsents = availableDataTypes.reduce((consents, dataType) => [
  ...consents,
  ...availablePurposes.map((purpose) => ({
    consentId: `sharing-${dataType}-${purpose}`,
    dataType,
    purpose,
  })),
], [] as Array<AvailableConsent>);

export default availableConsents;
