/* eslint-disable import/prefer-default-export */
import R from 'ramda';
import { DataType, Purpose } from '../../db/models';
import { consents as consentsStore } from '../../db';
import logger from '../../utils/logger';

const getActiveUsers = async (
  dataTypes: Array<DataType>, purpose: Purpose,
) => {
  const consentsPerDataType = await Promise.all(dataTypes.map((dataType) => consentsStore.query({
    dataType,
    purpose,
    active: true,
  })));
  const consents = R.flatten(consentsPerDataType);

  logger.debug(consents);

  const consentsByUserId = consents.reduce((acc, consent) => ({
    ...acc,
    [consent.userId]: [...(acc[consent.userId] || []), consent.dataType],
  }), {} as Record<string, Array<DataType>>);

  logger.debug(consentsByUserId);

  const users = Object.entries(consentsByUserId).filter(
    ([, userConsents]) => userConsents.length === dataTypes.length,
  ).map(([userId]) => userId);

  logger.debug(users);

  return users;
};

export const getSignedCertificate = async (dataTypes: Array<DataType>, purpose: Purpose) => {
  const activeUsers = await getActiveUsers(dataTypes, purpose);

  return {
    users: activeUsers,
    signature: 'TODO',
  };
};
