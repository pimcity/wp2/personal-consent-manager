import { consents as consentsStore } from '../../db';
import { Consent } from '../../db/models';
import { Services } from '../../services/definitions';
import { ConsentDTO } from '../../types/consents';

import availableConsents from './availableConsents';

export const getConsents = async (userId: string): Promise<Array<ConsentDTO>> => {
  const consents = await consentsStore.queryByIndex('userId', userId);
  const consentsMap = consents.reduce(
    (acc, consent) => ({ ...acc, [consent.consentId]: consent }),
    {} as { [consentId: string]: Consent | undefined},
  );
  return availableConsents.map((consent) => {
    const storedConsent = consentsMap[consent.consentId];
    return ({
      ...consent,
      active: storedConsent !== undefined ? storedConsent.active : false,
      date: storedConsent?.date,
    });
  });
};

export const updateConsent = async (
  userId: string,
  consentId: string,
  active: boolean,
  services: Services,
) => {
  const availableConsent = availableConsents.find((consent) => consent.consentId === consentId);
  if (!availableConsent) throw new Error('Unknown Consent ID');

  const id = `${consentId}-${userId}`;
  await Promise.all([
    consentsStore.upsert(id, {
      id,
      userId,
      ...availableConsent,
      active,
      date: Date.now(),
    }),
    services.taskManager.registerTask([userId], 'set-consents'),
  ]);
  return getConsents(userId);
};
