import { DataType } from '../../db/models';

const availableDataTypes: Array<DataType> = [
  'contact-information',
  'personal-information',
  'browsing-history',
  'location-history',
  'social-networks-data',
  'financial-data',
  'interests',
];

export default availableDataTypes;
