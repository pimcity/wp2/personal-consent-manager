import fs from 'fs';
import * as CSV from 'csv-string';

import { IABCategory } from '../../db/models';

const loadCSV = () => {
  const file = `${__dirname}/Content-Taxonomy-1.0.csv`;

  const rawData = fs.readFileSync(file, 'utf8');
  const parsedCSV = CSV.parse(rawData, ';');

  const categories = parsedCSV.slice(1);

  return categories.map((category) => ({
    iabCode: category[0],
    tier: category[1],
    category: category[2],
  }) as IABCategory);
};

const categories = loadCSV();

export default categories;
