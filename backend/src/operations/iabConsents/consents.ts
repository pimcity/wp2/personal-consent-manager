import { iabConsents as iabConsentsStore } from '../../db';
import { IABConsent } from '../../db/models';
import { IABConsentDTO } from '../../types/consents';

import availableConsents from './availableConsents';

export const getIABConsents = async (userId: string): Promise<Array<IABConsentDTO>> => {
  const consents = await iabConsentsStore.queryByIndex('userId', userId);
  const consentsMap = consents.reduce(
    (acc, consent) => ({ ...acc, [consent.iabCode]: consent }),
    {} as { [iabCode: string]: IABConsent | undefined},
  );
  return availableConsents.map((consent) => {
    const storedConsent = consentsMap[consent.iabCode];
    return ({
      ...consent,
      active: storedConsent !== undefined ? storedConsent.active : true,
      date: storedConsent?.date,
    });
  });
};

export const updateIABConsent = async (
  userId: string,
  iabCode: string,
  active: boolean,
) => {
  const availableConsent = availableConsents.find((consent) => consent.iabCode === iabCode);
  if (!availableConsent) throw new Error('Unknown IAB Code');

  const id = `${iabCode}-${userId}`;
  const { userId: theUserId, id: theId, ...updated } = await iabConsentsStore.upsert(id, {
    id,
    userId,
    ...availableConsent,
    active,
    date: Date.now(),
  });
  return updated;
};
