import Keycloak from 'keycloak-js';

import getConfig, { ProcessEnv } from '../getConfig';

let keycloak: Keycloak.KeycloakInstance;

const getKeycloak = (env: ProcessEnv) => {
  if (!keycloak) {
    const config = getConfig(env);
    keycloak = Keycloak(config.keycloak);
  }
  return keycloak;
};

export default getKeycloak;
