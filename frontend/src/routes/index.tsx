import Welcome from '../screens/Welcome';
import Dashboard from '../screens/Dashboard';
import Preferences from '../screens/Preferences';

import RoutesSkeleton from './RoutesSkeleton';

const Routes = () => (
  <RoutesSkeleton
    publicRoutes={[
      { path: '/welcome', Component: Welcome },
    ]}
    privateRoutes={[
      { path: '/dashboard', Component: Dashboard },
      { path: '/preferences', Component: Preferences },
    ]}
  />
);

export default Routes;
