import { useEffect } from 'react';
import {
  Redirect,
  Route, RouteComponentProps, useLocation,
} from 'react-router-dom';
import { useKeycloak } from '@react-keycloak/web';

import LoadingScreen from '../screens/LoadingScreen';

import TopBar from '../components/TopBar';
import LeftBar from '../components/LeftBar';

interface RouteProps {
  path: string;
  Component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
}

interface RoutesSkeletonProps {
  publicRoutes?: Array<RouteProps>;
  privateRoutes?: Array<RouteProps>;
}

const RoutesSkeleton = ({ publicRoutes, privateRoutes }: RoutesSkeletonProps) => {
  const basePath = (publicRoutes || privateRoutes)?.[0].path;
  if (!basePath) throw new Error('There must be at least one route');

  const { keycloak, initialized } = useKeycloak();
  const location = useLocation();

  const pathname = location.pathname.replace(/&.*/g, '');
  const privatePaths = privateRoutes?.map((s) => s.path);

  useEffect(() => {
    if (keycloak) {
      const { authenticated } = keycloak;
      const returnsFromKeycloak = location.pathname.includes('session_state');
      if (!authenticated && !returnsFromKeycloak && privatePaths?.includes(pathname)) {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        keycloak.login();
      }
    }
  }, [keycloak?.authenticated, location.pathname, pathname]);

  if (!initialized || !keycloak) return <LoadingScreen />;

  return (
    <div className="w-screen h-screen">
      {pathname === '/' && <Route path="/" render={() => <Redirect to={basePath} />} />}
      {publicRoutes?.map(({ path, Component }) => (
        <Route key={path} path={path} component={Component} />
      ))}

      {
        privateRoutes?.map(({ path, Component }) => (
          <Route
            key={path}
            render={({ location: loc, ...props }) => (loc.pathname.replace(/&.*/g, '') === path ? (
              <div className="flex flex-col w-full h-full">
                <TopBar />
                <div className="flex flex-1 w-full overflow-hidden">
                  <LeftBar />
                  <div className="flex-1 overflow-scroll">
                    <Component {...props} location={loc} />
                  </div>
                </div>
              </div>
            ) : null)}
          />
        ))
      }
    </div >
  );
};

export default RoutesSkeleton;
