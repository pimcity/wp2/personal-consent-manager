import styles from './Welcome.module.css';

import ExternalLink from '../components/ExternalLink';
import ButtonLink from '../components/ButtonLink';

import pdkLogo from '../images/pdkLogo.png';
import pimcityLogo from '../images/pimcityLogo.png';

const Welcome = () => (
  <div className={styles.welcome}>
    <nav className="flex items-center px-20 pt-4 pb-6 bg-primary-600">
      <img className="w-24 p-1 bg-white rounded" src={pimcityLogo} />
      <div className="flex-1 text-center">
        <h1 className="mb-1">PIMCity Demonstration</h1>
        <h4>BUILDING THE NEXT GENERATION PERSONAL DATA PLATFORMS</h4>
      </div>
    </nav>
    <div className="px-20 pt-12">
      <div className={styles.card}>
        <header>
          <h2>Personal Consent Manager</h2>
        </header>
        <section>
          <div className="flex justify-between mb-4">
            <img className="h-auto w-80" src={pdkLogo} />
            <div>
              <p>
                The <strong>Personal Consent Manager (P-CM)</strong> is the means to define all
                the user&apos;s privacy preferences. It defines which data a service is
                allowed to collect, process, or which can be shared with third parties by
                managing explicit consent. Users&apos; settings are imposed on
                all participating systems.
              </p>
              <p>
                Its primary objective is to give the users the transparency and control over
                their data in a GDPR compliant way. That is, give them the possibility to
                decide which data can be uploaded and stored in the platform, as well as
                how (raw, extracted or aggregated) data can be shared with Data Buyers
                in exchange for value when the opportunity arises.
              </p>
            </div>
          </div>
          <div className="flex justify-between mb-4">
            <div className="w-1/4 mr-3">
              <h3>Access the Web UI</h3>
              <p>Access the Personal Consent Manager Web UI.</p>
              <ButtonLink theme="secondary" to="/dashboard">
                Go to the online Demonstrator
              </ButtonLink>
            </div>
            <div className="w-1/4 mr-3">
              <h3>Access the API</h3>
              <p>See the OpenAPI definition of the Personal Consent Manager.</p>
              <ExternalLink theme="secondary" href="https://easypims.pimcity-h2020.eu/pcm-api/api-docs/">
                Go to the online Definition
              </ExternalLink>
            </div>
            <div className="w-1/4 mr-3">
              <h3>Source Code</h3>
              <p>The project is open-source and its code is on the online repository:</p>
              <ExternalLink theme="secondary" href="https://gitlab.com/pimcity/wp2/personal-consent-manager">GitLab</ExternalLink>
            </div>
            <div className="w-2/5">
              <h3>Licence</h3>
              <p>
                The P-CM is distributed under AGPL-3.0-only, see the LICENSE file in the project
                repository.
                Copyright (C) 2021  Wibson - Daniel Fernandez, Rodrigo Irarrazaval
              </p>
            </div>
          </div>
          <div className="flex justify-between mt-10 mb-4">
            <div className="w-5/12 pl-2 ml-24 mr-6">
              <h3>Demonstration video</h3>
              <span>In this video we show how to:</span>
              <ul className="pl-8 list-disc">
                <li>Log into the Personal-Consent Manager.</li>
                <li>
                  Visualize a summary of the Data Offers processed, missed,
                  and total value earned.
                </li>
                <li>Activate or deactivate consents for sharing data.</li>
              </ul>
            </div>
            <div className="w-6/12">
              <iframe
                width="560" height="315"
                src="https://www.youtube.com/embed/GINUMfkfkzc"
                title="Personal-Consent Manager"
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen>
              </iframe>
            </div>
          </div>
        </section>
        <footer>
          This project has received funding from the European Union Horizon 2020
          Research and Innovation programme under{' '}
          <a href="https://cordis.europa.eu/project/rcn/225755/factsheet/en">Grant Agreement No. 871370</a>
        </footer>
      </div>
    </div>
  </div >
);

export default Welcome;
