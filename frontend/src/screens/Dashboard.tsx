import { ArrowCircleRightIcon } from '@heroicons/react/solid';
import { Link } from 'react-router-dom';

import PrimaryCard from '../components/PrimaryCard';
import piggyBank from '../images/piggyBank.png';

const Dashboard = () => (
  <div className='p-5 pt-8'>
    <h1 className='mb-8 text-3xl font-medium'>Your Personal Stats</h1>
    <div className='flex justify-around'>
      <PrimaryCard
        title='Total Value Earned'
        img={piggyBank}
        msg='121 Euro Points'
      />
      <PrimaryCard title='Offers Processed' msg='53' />
      <PrimaryCard
        title='Missed Offers'
        msg='22'
        footMsg={(<Link className='flex font-medium text-secondary-500 hover:text-secondary-400' to='/preferences'>Share more data <ArrowCircleRightIcon className='w-5 ml-1' /></Link>)}
      />
    </div>
  </div>
);

export default Dashboard;
