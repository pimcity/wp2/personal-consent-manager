import { Fragment } from 'react';
import { Consent } from '../types/apiModels';
import { useGet, usePut } from '../hooks/useApi';

const dataTypesLabels = {
  'personal-information': 'Personal Information',
  'browsing-history': 'Browsing History',
  'location-history': 'Location History',
  'social-networks-data': 'Social Networks Data',
  'financial-data': 'Financial Data',
  interests: 'Interests',
} as const;

const ConsentPreferences = (
  { className, title, route }: { className?: string, title: string, route: string },
) => {
  const { data: consents, isLoading } = useGet<Array<Consent>>(route);
  const { mutate: submitConsent } = usePut<Partial<Consent>, Array<Consent>>(route);

  const setConsent = (consentId: string, active: boolean) => {
    submitConsent({ body: { consentId, active } });
  };

  return (
    <div className={className}>
      <h3 className='mb-8 text-2xl font-medium'>{title}</h3>
      {isLoading || !consents ? <h4>Loading...</h4> : (
        <div className='grid grid-cols-3 gap-4'>
          <h4 className='font-medium'>Data</h4>
          <h4 className='font-medium'>Purpose</h4>
          <h4 className='font-medium'>Active?</h4>
          {consents.map((consent) => (
            <Fragment key={consent.consentId}>
              <span>{dataTypesLabels[consent.dataType as keyof typeof dataTypesLabels]}</span>
              <span>{consent.purpose}</span>
              <input type='checkbox' checked={consent.active} onChange={(e) => setConsent(consent.consentId, e.target.checked)} />
            </Fragment>
          ))}
        </div>
      )}
    </div>
  );
};

const Preferences = () => (
  <div className='p-5 pt-8'>
    <h1 className='mb-8 text-3xl font-medium'>Your Consent Preferences</h1>
    <ConsentPreferences title="Data Shared with Third-parties" route="/consents" />
  </div>
);

export default Preferences;
