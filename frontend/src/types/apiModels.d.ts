export type DataType = 'adid' | 'geolocation' | 'gender' | 'age'
  | 'job' | 'interests' | 'fitness' | 'health' | 'onlinePurchases'
  | 'offlinePurchases' | 'creditRiskScore' | 'browsingHistory'
  | 'facebookPosts' | 'instagramFollowedPages' | 'instagramNumberOfFollowers'
  | 'twitterNumberOfFollowers' | 'twitterTweets';

export interface Consent {
  id: string; // db unique id
  userId: string;
  date: number;
  consentId: string;
  dataType: DataType;
  purpose: string;
  active: boolean;
}
