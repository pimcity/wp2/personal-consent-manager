import { useKeycloak } from '@react-keycloak/web';

import logoTransparent from '../images/pdkLogoTransparent.png';
import FloatingMenu from './FloatingMenu';

const TopBar = () => {
  const { keycloak } = useKeycloak();

  const menuOptions = [
    {
      label: 'Log out',
      onClick: () => keycloak.logout(),
    },
  ];

  return (
    <div className='flex items-center justify-between w-full pl-12 pr-4 bg-primary-600'>
      <img className="w-32 h-auto mb-1" src={logoTransparent} />
      <FloatingMenu menuOptions={menuOptions} />
    </div>
  );
};

export default TopBar;
