import { Link, useLocation } from 'react-router-dom';
import { LogoutIcon } from '@heroicons/react/outline';
import { useKeycloak } from '@react-keycloak/web';

const getText = (pathname: string, target: string) => (pathname === target ? 'text-primary-600' : 'text-primary-700 hover:text-primary-600');

const menu = [
  { label: 'Dashboard', to: '/dashboard' },
  { label: 'Preferences', to: '/preferences' },
];

const LeftBar = () => {
  const { keycloak } = useKeycloak();
  const { pathname } = useLocation();
  return (
    <div className='flex flex-col h-full px-2 pt-20 pb-4 text-center bg-gray-800 w-60'>
      {menu.map(({ label, to }) => (
        <div key={label}>
          <Link className={`text-xl font-bold ${getText(pathname, to)}`} to={to}>
            {label}
          </Link>
          <hr className="my-4" />
        </div>
      ))}
      <div className='flex items-end justify-center flex-grow'>
        <button className='flex items-center text-xl font-bold text-primary-700 hover:text-primary-600' onClick={() => keycloak.logout()}>
          <LogoutIcon className="w-6 mr-2" />
          Logout
        </button>
      </div>
    </div>
  );
};

export default LeftBar;
