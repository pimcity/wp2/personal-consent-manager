import { ReactElement } from 'react';

interface PrimaryCardProps {
  title: string;
  img?: string;
  msg: string | ReactElement;
  footMsg?: string | ReactElement;
}

const FootMsg = ({ footMsg }: { footMsg?: string | ReactElement }) => {
  if (!footMsg) return <></>;
  return typeof footMsg === 'string' ? (<h4>{footMsg}</h4>) : footMsg;
};

const PrimaryCard = ({
  title, img, msg, footMsg,
}: PrimaryCardProps) => (
  <div className='flex flex-col items-center p-4 rounded bg-primary-400 w-60'>
    <h2 className='text-xl font-medium'>{title}</h2>
    {img && <img className="flex-1 w-12 h-auto my-5" src={img} />}
    {
      typeof msg === 'string'
        ? <h3 className='flex items-center flex-1 text-2xl font-bold text-center text-white'>{msg}</h3>
        : msg
    }
    {footMsg && (
      <div className='h-6 mt-2'>
        <FootMsg footMsg={footMsg} />
      </div>
    )}
  </div>
);

export default PrimaryCard;
