Personal Consent Manager
===================

# Introduction

The primary objective of the Personal Consent Manager (P-CM) is to give the users the transparency and control over their data in a GDPR compliant way. That is, give them the possibility to decide which data can be uploaded and stored in the platform, as well as how (raw, extracted or aggregated) data can be shared with Data Buyers in exchange for value when the opportunity arises. 

The P-CM is presented as a web application and a REST API, not only providing users the possibility to use the component in a user-friendly way, but also enabling developers to integrate PIMCity Consent Management capabilities in their products. The architecture of the PDK is depicted in the figure below.

![image info](pcm.png)

# Installation

The documentation and the following instructions refer to a Linux environment, running with **Docker Engine v20.10.x** and **Docker Compose: v1.27.x**. The P-CM project has been cloned from [this GitLab repository](https://gitlab.com/pimcity/wp2/personal-consent-manager.git).

Follow accurately the next steps to quickly set-up the P-CM backbone on your server. All relevant steps are designed for a Linux machine, perform the equivalent procedure with other environments.

1. Prepare the environment:
```
> sudo apt-get update
> sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
> curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
> echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
> sudo apt-get update
> sudo apt-get install docker-ce docker-ce-cli containerd.io
```

2. Import the project from the GIT repository:
```
> git clone https://gitlab.com/pimcity/wp2/personal-consent-manager.git
```

# Usage
## Execution
To run the P-CM service, the following command should be executed at the root of the repository: 
```
> docker-compose up
```

## Configuration
The P-CM can be configured in three similar ways: 

1. Using environment variables. 
2. Defining those environment variables in a file called “.env” at the root directory of the folder with the PDK deployed, declaring them in the same way a variable is defined in the UNIX shell. You can check [this example](backend/.env.example).
3. Modifying [docker-compose.yml](docker-compose.yml) file.

## Usage Examples
You can check [here](https://easypims.pimcity-h2020.eu/pcm-api/api-docs/) the Swagger OpenAPI definition to see how the API is defined and used. Examples are provided as well.

## Authentication
KeyCloak service is used to carry out the authentication process. The user will not have to log in directly to the P-CM, but provide a JWT obtained from the authentication service or an EasyPIMS component, such as the PDA.

## Data Structure
For each data category and data sharing purpose, the user can enable or disable a specific consent. Therefore, the data structure used by the P-CM is essentially the tuple `(USER_ID, DATA_CATEGORY, DATA_SHARING_PURPOSE, IS_ACTIVE)`. More information is added to the consent, such as timestamps and so on, to provide an accurate service.

# License

The P-CM is distributed under AGPL-3.0-only, see the [LICENSE](LICENSE) file.

Copyright (C) 2021  Wibson - Daniel Fernandez, Rodrigo Irarrazaval
